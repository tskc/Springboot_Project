<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Tenant home</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="${contextPath}/resources/css/styling.css">
<style type="text/css"></style>
<script type="text/javascript"></script>

<style>

/* Set the size of the div element that contains the map */
#map {
	height: 400px; /* The height is 400 pixels */
	width: 90%; /* The width is the width of the web page */
	margin-left: 5%;
}
</style>
</head>
<body style="background: rgba(108, 146, 208, 0.5);">

	<div>
		<nav class="navbar" style="background: rgb(105, 145, 255)">
			<div class="container-fluid">
				<div>
					<img src="${contextPath}/picture white.png" class="logo" />
					<ul class="navbar-nav">
						<li>
							<form action="${contextPath}/welcome" method="get">
								<button type="submit" class="btn btn-primary navbar-btn">Home</button>
							</form>
						</li>
						<li>
							<form
								action="${contextPath}/viewTenantProfile/${pageContext.request.userPrincipal.name}"
								method="get">
								<button type="submit" class="btn btn-primary navbar-btn">My
									Profile</button>
							</form>
						</li>
						<li><c:if
								test="${pageContext.request.userPrincipal.name != null}">
								<form id="logoutForm" method="POST"
									action="${contextPath}/logout">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								</form>
								<button type="submit" class="btn btn-primary navbar-btn"
									onclick="document.forms['logoutForm'].submit()">Logout</button>
							</c:if></li>
					</ul>

				</div>
			</div>
		</nav>
		<h3 style="float: right; margin-top: -100px;">
			Welcome <b>${pageContext.request.userPrincipal.name}</b>
		</h3>
		<div>
			<form
				action="${contextPath}/${pageContext.request.userPrincipal.name}/propByCityAndType/search"
				method="get">
				Search by Flat Type: <select name="flatType">
					<option>No preference</option>
					<c:forEach items="${fType}" var="f">

						<option>${f}</option>
					</c:forEach>
				</select>&nbsp;&nbsp;&nbsp;&nbsp; Search by City: <input type="text"
					placeholder="Enter city" name="propertyCity" />
				&nbsp;&nbsp;&nbsp;&nbsp;

				<button type="submit" class="btn btn-primary" onclick="initMap()">Go</button>
			</form>
		</div>
		<c:if test="${empty property}">
			<b>No Properties found</b>
		</c:if>
		<!--The div element for the map -->
		<br> <br>
		<div id="map"></div>
		<input type="hidden" name="place" id="place" value="${propertyCity}" />
		<script>
			function initMap() {
				var a;
				var b;
				var uluru;
				var place = document.getElementById('place').value
						.toLowerCase();

				switch (place) {
				case "hyderabad":
					a = 17.3850;
					b = 78.4867;
					uluru = {
						lat : parseFloat(a),
						lng : parseFloat(b)
					};
					// The map, centered at Uluru
					var map = new google.maps.Map(document
							.getElementById('map'), {
						zoom : 10,
						center : uluru
					});
					// The marker, positioned at Uluru
					var marker = new google.maps.Marker({
						position : uluru,
						map : map
					});
					break;
				case "chennai":
					a = 13.0827;
					b = 80.2707;

					uluru = {
						lat : parseFloat(a),
						lng : parseFloat(b)
					};
					// The map, centered at Uluru
					var map = new google.maps.Map(document
							.getElementById('map'), {
						zoom : 10,
						center : uluru
					});
					// The marker, positioned at Uluru
					var marker = new google.maps.Marker({
						position : uluru,
						map : map
					});
					break;
				case "goa":
					a = 15.2993;
					b = 74.1240;

					uluru = {
						lat : parseFloat(a),
						lng : parseFloat(b)
					};
					// The map, centered at Uluru
					var map = new google.maps.Map(document
							.getElementById('map'), {
						zoom : 10,
						center : uluru
					});
					// The marker, positioned at Uluru
					var marker = new google.maps.Marker({
						position : uluru,
						map : map
					});
					break;
				case "kerala":
					a = 10.8505;
					b = 76.2711;

					uluru = {
						lat : parseFloat(a),
						lng : parseFloat(b)
					};
					// The map, centered at Uluru
					var map = new google.maps.Map(document
							.getElementById('map'), {
						zoom : 10,
						center : uluru
					});
					// The marker, positioned at Uluru
					var marker = new google.maps.Marker({
						position : uluru,
						map : map
					});
					break;
				case "mumbai":
					a = 19.0760;
					b = 72.8777;

					uluru = {
						lat : parseFloat(a),
						lng : parseFloat(b)
					};
					// The map, centered at Uluru
					var map = new google.maps.Map(document
							.getElementById('map'), {
						zoom : 10,
						center : uluru
					});
					// The marker, positioned at Uluru
					var marker = new google.maps.Marker({
						position : uluru,
						map : map
					});
					break;
				case "delhi":
					a = 28.7041;
					b = 77.1025;

					uluru = {
						lat : parseFloat(a),
						lng : parseFloat(b)
					};
					// The map, centered at Uluru
					var map = new google.maps.Map(document
							.getElementById('map'), {
						zoom : 10,
						center : uluru
					});
					// The marker, positioned at Uluru
					var marker = new google.maps.Marker({
						position : uluru,
						map : map
					});
					break;
				case "banglore":
					a = 12.97194;
					b = 77.59369;

					uluru = {
						lat : parseFloat(a),
						lng : parseFloat(b)
					};
					// The map, centered at Uluru
					var map = new google.maps.Map(document
							.getElementById('map'), {
						zoom : 10,
						center : uluru
					});
					// The marker, positioned at Uluru
					var marker = new google.maps.Marker({
						position : uluru,
						map : map
					});
					break;
				case "kolkata":
					a = 22.572645;
					b = 88.363892;

					uluru = {
						lat : parseFloat(a),
						lng : parseFloat(b)
					};
					// The map, centered at Uluru
					var map = new google.maps.Map(document
							.getElementById('map'), {
						zoom : 10,
						center : uluru
					});
					// The marker, positioned at Uluru
					var marker = new google.maps.Marker({
						position : uluru,
						map : map
					});
					break;
				default:
					a = 39.50;
					b = -98.35;
					uluru = {
						lat : parseFloat(a),
						lng : parseFloat(b)
					};
					// The map, centered at Uluru
					var map = new google.maps.Map(document
							.getElementById('map'), {
						zoom : 5,
						center : uluru
					});
					// The marker, positioned at Uluru
					var marker = new google.maps.Marker({
						position : uluru,
						map : map
					});
					break;
				}
			}
		</script>
		<script async defer
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCC4UktmznnXlQpaiTx6HSLkuvdq9255E&callback=initMap">
			
		</script>

		<br> <br> <br>
		<div class="container-fluid">
			<c:if test="${not empty property}">
				<center>
					<table class="table table-hover">
						<tr>
							<!-- 	<th>Id</th> -->
							<th>Property Name</th>
							<th>Flat Type</th>
							<th>Property Address</th>
							<th>Property City</th>
							<th>Property Area</th>
							<th>Landlord Name</th>
							<th>Rent per Month</th>
							<th>Facilities</th>
							<th>Property Image</th>
							<th>Action</th>
						</tr>
						<c:forEach items="${property}" var="p">
							<tr>
								<%-- <td>${p.propertyId}</td> --%>
								<td>${p.propertyName}</td>
								<td>${p.flatType}</td>
								<td>${p.propertyAddress}</td>
								<td>${p.propertyCity}</td>
								<td>${p.propertyArea}</td>
								<td>${p.landlordName}</td>
								<td>${p.rentPerMonth}</td>
								<td align="left"><c:forEach var="i" items="${p.facilities}">
										<ul>${i}</ul>
									</c:forEach></td>
								<td><img src="${contextPath}/images/${p.image}"
									style="height: 100px; width: 150px; image-rendering: pixelated"></td>
								<td><form
										action="${contextPath}/request/${p.propertyId}/${pageContext.request.userPrincipal.name}"
										method="get">
										<button type="submit" class="btn btn-primary">Request</button>
									</form></td>
							</tr>
						</c:forEach>
					</table>
				</center>
			</c:if>
		</div>
	</div>
</body>
</html>