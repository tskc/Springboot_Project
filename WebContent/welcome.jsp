<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Home</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="${contextPath}/resources/css/styling.css">
<style type="text/css">
body {
	image-rendering: pixelated;
	background:
		url("https://images.pexels.com/photos/186077/pexels-photo-186077.jpeg");
	background-size: cover;
}
</style>
</head>
<body>
	<div>
		<nav class="navbar" style="background: rgb(105, 145, 255)">
			<div class="container-fluid">
				<div>
					<img src="${contextPath}/picture white.png" class="logo" />
					<ul class="navbar-nav">
						<!--<button class="btn btn-danger navbar-btn">Button</button> -->
						<li>
							<form action="${contextPath}/welcome" method="get">
								<button type="submit" class="btn btn-primary navbar-btn">Home</button>
							</form>
						</li>

						<li><c:if
								test="${pageContext.request.userPrincipal.name != null}">
								<form id="logoutForm" method="POST"
									action="${contextPath}/logout">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								</form>
								<button type="submit" class="btn btn-primary navbar-btn"
									onclick="document.forms['logoutForm'].submit()">Logout</button>
							</c:if></li>
					</ul>
				</div>
			</div>
		</nav>

		<h1 style="float: right; margin-top: -20px;">
			Welcome <b>${pageContext.request.userPrincipal.name}</b>
		</h1>
		<br> <br> <br> <br> <br>

		<div class="container">
			<div align="center" style="margin-top: -40px;">
				<strong>
					<h1 style="color: rgb(0, 0, 0);">Choose your role</h1>
				</strong>
			</div>
			<ul class="role">
				<li>
					<form
						action="${contextPath}/OtherProperties/${pageContext.request.userPrincipal.name}/name"
						method="get">
						<button type="submit" class="btn btn-dark" id="properties">Tenant</button>
					</form>

					<form action="landlord-home" method="get">
						<button type="submit" class="btn btn-dark">Landlord</button>
					</form>
				</li>
			</ul>
		</div>
	</div>
</body>
</html>
