
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Edit Profile</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="${contextPath}/resources/css/styling.css">
<style type="text/css"></style>
</head>
<body>
	<div>
		<nav class="navbar" style="background: rgb(105, 145, 255)">
			<div class="container-fluid">
				<div>
					<img src="${contextPath}/picture white.png" class="logo" />
					<ul class="navbar-nav">
						<li>
							<form action="${contextPath}/welcome" method="get">
								<button type="submit" class="btn btn-primary navbar-btn">Home</button>
							</form>
						</li>
						<li><c:if
								test="${pageContext.request.userPrincipal.name != null}">
								<form id="logoutForm" method="POST"
									action="${contextPath}/logout">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								</form>
								<button type="submit" class="btn btn-primary navbar-btn"
									onclick="document.forms['logoutForm'].submit()">Logout</button>
							</c:if></li>
					</ul>

				</div>
			</div>
		</nav>
		<h2 style="float: right; margin-right: 30px;">
			Welcome <b> ${pageContext.request.userPrincipal.name}</b>
		</h2>
		<br> <br> <br> <br> <br> <br>
		<div align="center">
			<form:form
				action="${contextPath}/${pageContext.request.userPrincipal.name}/editUser?${_csrf.parameterName}=${_csrf.token}"
				modelAttribute="user" method="post" enctype="multipart/form-data">
				<table>
					<tr>


						<td><form:input type="hidden" path="id" id="id"
								required="required" readonly="true" /></td>
						<form:errors path="id" />

					</tr>
					<tr>

						<td><form:label path="username">Username </form:label></td>
						<td>:</td>
						<td><form:input type="text" path="username" id="username"
								required="required" readonly="true" /></td>
						<form:errors path="username" />

					</tr>


					<tr>

						<td><form:label path="city">City  </form:label></td>
						<td>:&nbsp;</td>
						<td><form:input type="text" path="city" id="city"
								required="required" /></td>
						<form:errors path="city" />

					</tr>
					<tr>

						<td><form:label path="emailId">Email-ID  </form:label></td>
						<td>:</td>
						<td><form:input type="email" path="emailId" id="emailId"
								required="required" /></td>
						<form:errors path="emailId" />

					</tr>
					<tr>

						<td><input type="file" name="file" required="required" /></td>

					</tr>
				</table>
				<div>

					<div>
						<input type="submit" value="Update User" class="btn btn-primary">
					</div>
				</div>
			</form:form>
		</div>
	</div>
</body>
</html>