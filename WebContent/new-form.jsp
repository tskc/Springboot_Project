<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add property</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="${contextPath}/resources/css/styling.css">
<style type="text/css"></style>

</head>
<body>
	<div>
		<nav class="navbar" style="background: rgb(105, 145, 255)">
			<div class="container-fluid">
				<div>
					<img src="${contextPath}/picture white.png" class="logo" />
					<ul class="navbar-nav">
						<li>
							<form action="${contextPath}/welcome" method="get">
								<button type="submit" class="btn btn-primary navbar-btn">Home</button>
							</form>
						</li>
						<li>
							<form
								action="${contextPath}/viewProfileLandlord/${pageContext.request.userPrincipal.name}"
								method="get">
								<button type="submit" class="btn btn-primary navbar-btn">My
									Profile</button>
							</form>
						</li>
						<li><c:if
								test="${pageContext.request.userPrincipal.name != null}">
								<form id="logoutForm" method="POST"
									action="${contextPath}/logout">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								</form>
								<button type="submit" class="btn btn-primary navbar-btn"
									onclick="document.forms['logoutForm'].submit()">Logout</button>
							</c:if></li>
					</ul>

				</div>
			</div>
		</nav>
		<h3 style="float: right; margin-right: 20px;">
			Welcome <b>${pageContext.request.userPrincipal.name}</b>
		</h3>

		<br> <br> <br>
		<h2 style="font-size: 40px; margin-left: 600px">
			<b>Add a Property</b>
		</h2>
		<div align="center" style="table-layout: auto;">

			<form:form
				action="${contextPath}/${pageContext.request.userPrincipal.name}/add?${_csrf.parameterName}=${_csrf.token}"
				method="post" modelAttribute="property"
				enctype="multipart/form-data">
				<table>
					<tr>

						<td><form:label path="propertyName">Property Name </form:label></td>
						<td>:</td>
						<td><form:input id="propertyName" type="text"
								path="propertyName" required="required" /></td>
						<form:errors path="propertyName"></form:errors>
					</tr>

					<tr>

						<td><form:label path="flatType">Flat Type </form:label></td>
						<td>:</td>
						<td><form:select path="flatType">
								<form:option value="1BHK">1BHK</form:option>
								<form:option value="2BHK">2BHK</form:option>
								<form:option value="3BHK">3BHK</form:option>
								<form:option value="4BHK">4BHK</form:option>
							</form:select></td>
						<form:errors path="flatType"></form:errors>

					</tr>

					<tr>

						<td><form:label path="propertyAddress">Property Address</form:label></td>
						<td>:</td>
						<td><form:input path="propertyAddress" type="text"
								id="propertyAddress" required="required" /></td>
						<form:errors path="propertyAddress"></form:errors>

					</tr>

					<tr>

						<td><form:label path="propertyCity">Property City </form:label></td>
						<td>: &nbsp;</td>
						<td><form:input path="propertyCity" type="text"
								id="propertyCity" required="required" /></td>
						<form:errors path="propertyCity"></form:errors>

					</tr>

					<tr>

						<td><form:label path="propertyArea">Property Area </form:label></td>
						<td>:</td>
						<td><form:input path="propertyArea" type="number"
								id="propertyArea" required="required" min="0" /></td>
						<form:errors path="propertyArea"></form:errors>

					</tr>


					<tr>

						<td><form:label path="landlordName">Landlord Name </form:label></td>
						<td>:</td>
						<td><form:input path="landlordName" type="text"
								id="landlordName"
								value="${pageContext.request.userPrincipal.name}"
								readonly="true" /></td>
						<form:errors path="landlordName"></form:errors>
					<tr>
						<td><form:label path="rentPerMonth">Rent Per Month </form:label></td>
						<td>:</td>
						<td><form:input path="rentPerMonth" type="number"
								id="rentPerMonth" min="0" /></td>
						<form:errors path="rentPerMonth"></form:errors>

					</tr>

					<tr>

						<td><form:label path="facilities">Facilities </form:label></td>
						<td>:</td>
						<td><form:checkbox path="facilities" value="Swimming Pool" />
							Swimming Pool <form:checkbox path="facilities" value="Wi-Fi" />
							Wi-Fi <form:checkbox path="facilities" value="Lift" /> Lift <form:checkbox
								path="facilities" value="Gym" /> Gym</td>
						<form:errors path="facilities"></form:errors>

					</tr>



					<tr>
						<td><input type="file" name="file" required="required" /></td>
					</tr>



					<tr align="center">
						<td><button type="submit" class="btn btn-primary">Add
								Property</button></td>
					</tr>
				</table>
			</form:form>

		</div>
	</div>
</body>
</html>