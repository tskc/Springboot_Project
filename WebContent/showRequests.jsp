<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Requests</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="${contextPath}/resources/css/styling.css">
<style type="text/css"></style>
</head>
<body>
	<div>
		<nav class="navbar" style="background: rgb(105, 145, 255)">
			<div class="container-fluid">
				<div>
					<img src="${contextPath}/picture white.png" class="logo" />
					<ul class="navbar-nav">
						<li>
							<form action="${contextPath}/welcome" method="get">
								<button type="submit" class="btn btn-primary navbar-btn">Home</button>
							</form>
						</li>
						<li>
							<form
								action="${contextPath}/viewProfileLandlord/${pageContext.request.userPrincipal.name}"
								method="get">
								<button type="submit" class="btn btn-primary navbar-btn">My
									Profile</button>
							</form>
						</li>
						<li><c:if
								test="${pageContext.request.userPrincipal.name != null}">
								<form id="logoutForm" method="POST"
									action="${contextPath}/logout">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								</form>
								<button type="submit" class="btn btn-primary navbar-btn"
									onclick="document.forms['logoutForm'].submit()">Logout</button>
							</c:if></li>
					</ul>

				</div>
			</div>
		</nav>
		<h2 style="float: right; margin-right: 30px;">
			Welcome <b>${pageContext.request.userPrincipal.name}</b>
		</h2>
		<br> <br>
		<h3>Requests from Tenants</h3>
		<c:if test="${empty requests}">
			<b>There aren't any requests for this property</b>
		</c:if>
		<c:if test="${not empty requests}">
			<table class="table table-hover">
				<tr>
					<th>Name</th>
					<th>Action</th>
				</tr>
				<tr>
					<c:forEach var="r" items="${requests}">
						<div>
							<td>${r.userName}</td>
							<td><form
									action="${contextPath}/viewTenantProfile/${r.userName}"
									method="get">
									<button type="submit" class="btn btn-primary">Tenant
										Profile</button>
								</form></td>
						</div>
				</tr>
				</c:forEach>
			</table>
		</c:if>
	</div>
</body>
</html>