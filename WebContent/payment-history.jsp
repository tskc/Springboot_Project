<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Payments history</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="${contextPath}/resources/css/styling.css">
<style type="text/css"></style>
</head>
<body>
	<div>
		<nav class="navbar" style="background: rgb(105, 145, 255)">
			<div class="container-fluid">
				<div>
					<img src="${contextPath}/picture white.png" class="logo" />
					<ul class="navbar-nav">
						<li>
							<form action="${contextPath}/welcome" method="get">
								<button type="submit" class="btn btn-primary navbar-btn">Home</button>
							</form>
						</li>
						<li>
							<form
								action="${contextPath}/viewProfileLandlord/${pageContext.request.userPrincipal.name}"
								method="get">
								<button type="submit" class="btn btn-primary navbar-btn">My
									Profile</button>
							</form>
						</li>
						<li><c:if
								test="${pageContext.request.userPrincipal.name != null}">
								<form id="logoutForm" method="POST"
									action="${contextPath}/logout">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								</form>
								<button type="submit" class="btn btn-primary navbar-btn"
									onclick="document.forms['logoutForm'].submit()">Logout</button>
							</c:if></li>
					</ul>

				</div>
			</div>
		</nav>
		<h2 style="float: right; margin-right: 30px;">
			Welcome <b>${pageContext.request.userPrincipal.name}</b>
		</h2>
		<br> <br>
		<div>
			<c:if test="${empty plist}">

				<tr>
					<td>
						<div align="center">
							<h2>No Payments Found</h2>

						</div>
					<td>
				</tr>
			</c:if>
			<c:if test="${not empty plist}">
				<table class="table table-hover">
					<tr>
						<th>Name on the Card</th>
						<th>Amount Paid</th>
						<th>Card Type</th>
						<th>Date and Time</th>
					</tr>
					<c:forEach items="${plist}" var="p">
						<tr>
							<td>${p.name }</td>
							<td>${p.amountpaid }</td>
							<td>${p.cardtype }</td>
							<td>${p.date}</td>
						</tr>
					</c:forEach>
				</table>
			</c:if>
		</div>
	</div>
</body>
</html>