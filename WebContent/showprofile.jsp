<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>View Profile</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="${contextPath}/resources/css/styling.css">
<style type="text/css"></style>

<script type="text/javascript">
	function statement() {
		alert("This payment is for hosting service");
	}
</script>
</head>
<body>


	<div>
		<nav class="navbar" style="background: rgb(105, 145, 255)">
			<div class="container-fluid">
				<div>
					<img src="${contextPath}/picture white.png" class="logo" />
					<ul class="navbar-nav">
						<li>
							<form action="${contextPath}/welcome" method="get">
								<button type="submit" class="btn btn-primary navbar-btn">Home</button>
							</form>
						</li>
						<li><c:if
								test="${pageContext.request.userPrincipal.name != null}">
								<form id="logoutForm" method="POST"
									action="${contextPath}/logout">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								</form>
								<button type="submit" class="btn btn-primary navbar-btn"
									onclick="document.forms['logoutForm'].submit()">Logout</button>
							</c:if></li>
					</ul>

				</div>
			</div>
		</nav>
		<h2 style="float: right; margin-right: 30px;">
			Welcome <b>${pageContext.request.userPrincipal.name}</b>
		</h2>
		<br> <br> <br> <br> <br>
		<c:if test="${empty Landlord }">
			<c:if test="${not empty Tenant}">
				<div align="center">
					<table>

						<img src="${contextPath}/images/${Tenant.image}"
							style="height: 100px; width: auto; image-rendering: pixelated">


						<tr>
							<td>Name</td>
							<td>:&nbsp;</td>
							<td>${Tenant.username}</td>
						</tr>
						<tr>
							<td>City</td>
							<td>:</td>
							<td>${Tenant.city}</td>
						</tr>
						<tr>
							<td>Email-ID</td>
							<td>:</td>
							<td>${Tenant.emailId}</td>
						</tr>

					</table>
				</div>
				<c:if
					test="${pageContext.request.userPrincipal.name eq Tenant.username}">
					<%-- <form
						action="${contextPath}/editUserProfile/${pageContext.request.userPrincipal.name}"
						method="get">
						<button type="submit" class="btn btn-primary">edit
							profile</button>
					</form> --%>
					<div align="center" style="list-style: none;">
						<form
							action="${contextPath}/editUserProfile/${pageContext.request.userPrincipal.name}"
							method="get">
							<button type="submit" class="btn btn-primary">Edit
								Profile</button>
						</form>
					</div>
				</c:if>
			</c:if>
			<c:if test="${empty Tenant}">
	No tenants for this property
	</c:if>

		</c:if>

		<c:if test="${not empty Landlord}">
			<div align="center">
				<table class="table-hover">
					<img src="${contextPath}/images/${Landlord.image}"
						style="height: 100px; width: auto; image-rendering: pixelated">


					<tr>
						<td>Name</td>
						<td>:</td>
						<td>${Landlord.username}</td>
					</tr>
					<tr>
						<td>City</td>
						<td>:</td>
						<td>${Landlord.city}</td>
					</tr>
					<tr>
						<td>Email-ID</td>
						<td>:&nbsp;</td>
						<td>${Landlord.emailId}</td>
					</tr>

				</table>
			</div>
			<br>
			<br>
			<c:if
				test="${pageContext.request.userPrincipal.name eq Landlord.username}">
				<div align="center" style="list-style: none;">
					<form
						action="${contextPath}/editUserProfile/${pageContext.request.userPrincipal.name}"
						method="get">
						<button type="submit" class="btn btn-primary">Edit
							Profile</button>
					</form>
					<br>
					<form
						action="${contextPath}/makePayment/${pageContext.request.userPrincipal.name}"
						method="get" onclick="statement()" onkeypress="statement()">
						<button type="submit" class="btn btn-primary">Make
							Payment</button>
					</form>
				</div>
				<br>
				<div align="center">
					<form
						action="${contextPath}/paymenthistory/${pageContext.request.userPrincipal.name}"
						method="get">
						<button type="submit" class="btn btn-primary">Payments
							History</button>
					</form>
				</div>
			</c:if>
		</c:if>
	</div>
</body>
</html>