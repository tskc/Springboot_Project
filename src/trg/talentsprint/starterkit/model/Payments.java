package trg.talentsprint.starterkit.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Payments {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int amountpaid;
	private String cardtype;
	private String user;
	private String date;
	private String name;
	public Payments() {
		super();
	}
	public Payments(int id, int amountpaid, String cardtype, String user, String date, String name) {
		super();
		this.id = id;
		this.amountpaid = amountpaid;
		this.cardtype = cardtype;
		this.user = user;
		this.date = date;
		this.name = name;
	}
	public Payments(int amountpaid, String cardtype, String user, String date, String name) {
		super();
		this.amountpaid = amountpaid;
		this.cardtype = cardtype;
		this.user = user;
		this.date = date;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAmountpaid() {
		return amountpaid;
	}
	public void setAmountpaid(int amountpaid) {
		this.amountpaid = amountpaid;
	}
	public String getCardtype() {
		return cardtype;
	}
	public void setCardtype(String cardtype) {
		this.cardtype = cardtype;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
