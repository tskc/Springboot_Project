package trg.talentsprint.starterkit.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import trg.talentsprint.starterkit.model.Property;
import trg.talentsprint.starterkit.repository.PropertyRepository;

@Service
public class PropertyService {
	@Autowired
	private PropertyRepository repo;

	// cRud
	public List<Property> allProperty() {
		return (List<Property>) repo.findAll();
	}

	public Optional<Property> getPropertyById(int id) {
		return repo.findById(id);
	}

	// Crud
	public Property addProperty(Property p) {
		return repo.save(p);
	}

	// cruD
	public void deletePropertyById(int id) {
		repo.deleteById(id);

	}

	// crUd
	public int updateProperty(String flatType, String propertyAddress, String propertyName, int propertyArea,String propertyCity, long rentPerMonth, String[] facilities, int propertyId) {

		return repo.propertyUpdate(flatType, propertyAddress, propertyName, propertyArea, propertyCity, rentPerMonth,facilities,propertyId);
	}

	public List<Property> getOtherUserProperties(String user) {
		return repo.propertyOfDiffUsers(user);
	}

	public List<String> getPropertyCities() {
		return repo.getPropertyCities();
	}

	public List<String> getFlatTypes() {
		return repo.getFlatTypes();
	}

	public List<Property> getPropertyByCityOrType(String flatType, String propertyCity, String username) {
		return repo.getPropertyByCityOrType(flatType,propertyCity,username);
	}

	public List<Property> myProperties(String name) {
		// TODO Auto-generated method stub
		return repo.getPropertyByName(name);
	}

	/*
	 * public List<Property> allPropertyDifferentLandlordsProperties() { return
	 * repo.findExceptLandlord(); }
	 */
}
