package trg.talentsprint.starterkit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import trg.talentsprint.starterkit.model.Property;

@Repository
public interface PropertyRepository extends CrudRepository<Property, Integer>{

	
	@Transactional
	@Modifying
	@Query(value= "update property set flat_type=?1,property_address=?2,property_name=?3,property_area=?4,property_city=?5,rent_per_month=?6,facilities=?7 where property_id=?8",nativeQuery = true)
	int propertyUpdate(String flatType, String propertyAddress, String propertyName, int propertyArea, String propertyCity, long rentPerMonth, String[] facilities, int propertyId);
	
	
	@Query(value="select * from property where landlord_name!=?1", nativeQuery = true)
	List<Property> propertyOfDiffUsers(String user);

	@Query(value="select distinct property_city from property", nativeQuery = true)
	List<String> getPropertyCities();

	@Query(value = "select distinct flat_type from property", nativeQuery = true)
	List<String> getFlatTypes();

	@Query(value = "select * from property where landlord_name!=?3 and (flat_type=?1 or property_city=?2)",nativeQuery = true)
	List<Property> getPropertyByCityOrType(String flatType, String propertyCity, String username);

	@Query(value = "select * from property where landlord_name=?1", nativeQuery = true)
	List<Property> getPropertyByName(String name);


	/*
	 * @Transactional
	 * 
	 * @Modifying
	 * 
	 * @Query(value =
	 * "update property set flat_type=?1,property_address=?2,property_name=?3,rent_per_month=?4 where property_id=?5"
	 * ,nativeQuery = true) int propertyUpdate(String flat_type, String
	 * property_address,String property_name, long rent_per_month, String
	 * propertyCity, long rentPerMonth, int property_id);
	 */
	
}
