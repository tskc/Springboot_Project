package trg.talentsprint.starterkit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import trg.talentsprint.starterkit.model.Payments;

@Repository
public interface PaymentsRepository extends CrudRepository<Payments, Integer> {
	@Modifying
	@Transactional
	@Query(value = "insert into payments(amountpaid,cardtype,user,date,name) values (?1,?2,?3,?4,?5)", nativeQuery = true)
	void paymentsave(int amount, String type, String user, String date, String name);

	@Query(value= "select * from payments where user=?1", nativeQuery = true)
	List<Payments> myPayments(String user);

}
