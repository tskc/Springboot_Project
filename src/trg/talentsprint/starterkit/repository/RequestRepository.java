package trg.talentsprint.starterkit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import trg.talentsprint.starterkit.model.Requests;

@Repository
public interface RequestRepository extends CrudRepository<Requests, Integer>{
	
	@Query(value = "select * from requests where property_id=?1", nativeQuery = true)
	List<Requests> getRequests(int id);

}
