package trg.talentsprint.starterkit.web;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import trg.talentsprint.starterkit.model.User;
import trg.talentsprint.starterkit.service.SecurityService;
import trg.talentsprint.starterkit.service.UserService;
import trg.talentsprint.starterkit.validator.UserValidator;

@Controller
public class UserController {
	@Autowired
	private UserService userService;

	@Autowired
	private SecurityService securityService;

	@Autowired
	private UserValidator userValidator;

	@Value("${upload.location}")
	private String uploadDirectory;

	@GetMapping("/registration")
	public String registration(Model model) {
		model.addAttribute("userForm", new User());

		return "registration";
	}

	@PostMapping(value = "/registration", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model,
			@RequestParam("file") MultipartFile[] files) {
		userValidator.validate(userForm, bindingResult);
		StringBuilder fileNames = new StringBuilder();
		for (MultipartFile file : files) {
			Path fileNameAndPath = Paths.get(uploadDirectory, file.getOriginalFilename());
			fileNames.append(file.getOriginalFilename() + " ");
			try {
				Files.write(fileNameAndPath, file.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String path = fileNames.toString();
		userForm.setImage(path);

		userValidator.validate(userForm, bindingResult);
		if (bindingResult.hasErrors()) {
			return "registration";
		}

		userService.save(userForm);

		securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

		return "redirect:/welcome";
	}

	@GetMapping("/login")
	public String login(Model model, String error, String logout) {
		if (error != null)
			model.addAttribute("error", "Your username and password is invalid.");

		if (logout != null)
			model.addAttribute("message", "You have been logged out successfully.");

		return "login";
	}

	@GetMapping({ "/", "/welcome" })
	public String welcome(Model model) {
		return "welcome";
	}

	@PostMapping(value = "/{userName}/editUser", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String updateTenant(@Valid @ModelAttribute("user") User user, @RequestParam("username") String userName,
			@RequestParam("id") int id,@RequestParam("emailId") String email, @RequestParam("city") String city,
			@RequestParam("file") MultipartFile[] files, BindingResult bindingResult, Model model) {
		StringBuilder fileNames = new StringBuilder();
		for (MultipartFile file : files) {
			Path fileNameAndPath = Paths.get(uploadDirectory, file.getOriginalFilename());
			fileNames.append(file.getOriginalFilename() + " ");
			try {
				Files.write(fileNameAndPath, file.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String path = fileNames.toString();
		user.setImage(path);
		userService.updateDetails(email, city, path, userName);
		model.addAttribute("Landlord",userService.findByUsername(userName));
		System.out.println("done");
		return "showprofile";
	}

	@GetMapping("/viewProfileLandlord/{name}")
	public String viewProfileLandlord(Model model, @PathVariable("name") String name) {
		model.addAttribute("Landlord", userService.findByUsername(name));
		return "showprofile";
	}

	@GetMapping("/editUserProfile/{name}")
	public String editUserProfile(Model model, @PathVariable("name") String name) {
		model.addAttribute("user", userService.findByUsername(name));
		return "editprofile";

	}
	
	@GetMapping("/viewTenantProfile/{name}")
	public String viewTenantProfile(Model model, @PathVariable("name") String name) {
		model.addAttribute("Tenant", userService.findByUsername(name));
		return "showprofile";
	}

}
